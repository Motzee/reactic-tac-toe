import React, { Component } from 'react';
import './Timer.css';
import EventsManager from './EventsManager' ;

const Time = 6000 ;
    
class Timer extends React.Component {
    
    constructor(props) {
        super(props);
        this.state = {
            timeLeft: Time
        };
        this.timer = null ;
        EventsManager.addListener('someonePlay', () => {
            clearInterval(this.timer) ;
            this.setState({
                timeLeft: Time
            });
            this.start() ;     
        }) ;
    }

    //lancer un décompte, et s'il atteint 0 on récursive
    start() {
        var grid = document.getElementsByClassName("square") ;
        for (let i = 0; i < grid.length; i++) {
            grid[i].style.backgroundColor = "white";
        }
        this.timer = setInterval(() => {
            this.setState({
                timeLeft: this.state.timeLeft - 1000
            });
            if(this.state.timeLeft < 0) {
                clearInterval(this.timer) ;
                EventsManager.emit('tooLate', true) ;
                this.setState({
                    timeLeft: Time
                });
                this.start() ;
            }
        }, 1000);
    }
    
    //si clic, appel de...
   reset(timer) {
        this.stop() ;
        this.setState({
            timeLeft: Time
        });
        EventsManager.emit('reset', true) ;
    }
   
    stop() {
        clearInterval(this.timer) ;
        var grid = document.getElementsByClassName("square") ;
        for (let i = 0; i < grid.length; i++) {
            grid[i].style.backgroundColor = "hsl(0, 0%, 30%)";
        }
    }
    
    render() {
        let secLeft = this.state.timeLeft / 1000 ;
        return (
            <div>
                <p>Time left: {secLeft} sec</p>
                <Button value="Start" onClick={() => this.start()} />
                <Button value="Pause" onClick={() => this.stop()} />
                <Button value="Reset" onClick={() => this.reset()} />
            </div>
        );
    }
}
    
export default Timer ;
    
    
class Button extends React.Component {
    render() {
        return (
            <button onClick={this.props.onClick}>{this.props.value}</button>
        );
    }
}